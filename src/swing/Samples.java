package swing;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 * Sample on using simple Swing components for ICS purposes
 * 
 * @author = Mandeep Sran
 * @date = Oct. 7th, 2013
 **/

public class Samples extends JFrame {

	private Container container;
	private JLabel titleLabel;
	private JLabel field1Label;
	private JTextField field1;
	private JRadioButton rButton1;
	private JRadioButton rButton2;
	private ButtonGroup buttonGroup;
	private JButton button1;

	/** Constructor for creating Samples object **/
	public Samples() {
		setSize(300, 200);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		container = getContentPane();
		container.setLayout(null);
		// titleLabel
		titleLabel = new JLabel("Sample Program");
		titleLabel.setSize(100, 20);
		titleLabel.setLocation(100, 10);
		container.add(titleLabel);
		// field1Label
		field1Label = new JLabel("My JTextField is Here: ");
		field1Label.setSize(130, 20);
		field1Label.setLocation(25, 50);
		container.add(field1Label);
		// field1
		field1 = new JTextField("Insert Text", 20);
		field1.setSize(100, 20);
		field1.setLocation(155, 50);
		container.add(field1);
		// rButton1
		rButton1 = new JRadioButton("First Option");
		rButton1.setSize(100, 20);
		rButton1.setLocation(20, 80);
		rButton1.setSelected(true);
		container.add(rButton1);
		// rButton2
		rButton2 = new JRadioButton("Second Option");
		rButton2.setSize(120, 20);
		rButton2.setLocation(120, 80);
		container.add(rButton2);
		// buttonGroup
		buttonGroup = new ButtonGroup();
		buttonGroup.add(rButton1);
		buttonGroup.add(rButton2);
		// button1
		button1 = new JButton("Disable JTextField");
		button1.setSize(150, 25);
		button1.setLocation(80, 120);
		container.add(button1);
		// Adding Action Listener
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Disables/Enables JTextField
				if (field1.isEnabled()) {
					field1.setEnabled(false);
				} else {
					field1.setEnabled(true);
				}
			}
		});
		setVisible(true);
	}

	/** Main class to run program **/
	public static void main(String[] args) {
		new Samples();
	}
}
